package Business::OnlineThirdPartyPayment::Dummy;

use strict;
use Carp;
use Business::OnlineThirdPartyPayment 3;
use Business::CreditCard;
use vars qw($VERSION @ISA $DEBUG);

@ISA = qw(Business::OnlineThirdPartyPayment);
$VERSION = '0.01';

$DEBUG = 0;

sub get_fields {
    my($self,@fields) = @_;

    my %content = $self->content();
    my %new = ();
    foreach( grep defined $content{$_}, @fields) { $new{$_} = $content{$_}; }
    return %new;
}

sub reference {
    my ($self, $data) = @_;
    $data->{reference} || '';
}

sub submit {
    my($self) = @_;
    my %content = $self->content;

    my $action = lc($content{'action'});
    die 'Dummy only supports "Authorization Only" and "Post Authorization" transactions'
      unless $action eq 'authorization only' || $action eq 'post authorization';

    my @required = qw( login amount );
    push @required, qw( reference ) if $action eq 'post authorization';
    if ($self->transaction_type() eq 'CC' ) {
      push @required, qw( name ) unless $action eq 'post authorization';
    }elsif ($self->transaction_type() eq 'ECHECK' ) {
      push @required, qw( account_name ) unless $action eq 'post authorization';
    } else {
      croak("Dummy can't handle transaction type: ".
            $self->transaction_type());
    }
    $self->required_fields(@required);

    $self->popup_url( "http://127.0.0.1/webpay/collect.cgi?".
                      join('&', map { "$_=". $content{$_} }
                                qw( reference amount redirecturl )
                      )
                    );

    $self->is_success(1);
    $self->authorization('Authorized');
}

1;
__END__

=head1 NAME

Business::OnlineThirdPartyPayment::Dummy - dummy backend for Business::OnlineThirdPartyPayment

=head1 SYNOPSIS

  use Business::OnlineThirdPartyPayment;

  my $tx = new Business::OnlineThirdPartyPayment("Dummy");
  $tx->content(
      login          => '87654321', 
      action         => 'Normal Authorization',
      description    => 'Business::OnlinePayment test',
      amount         => '49.95',
      invoice_number => '100100',
      name           => 'Tofu Beast',
      card_number    => '46464646464646',
      expiration     => '11/08',
  );
  $tx->submit();

  if($tx->is_success()) {
      print "Card processed successfully: ".$tx->authorization."\n";
  } else {
      print "Card was rejected: ".$tx->error_message."\n";
  }

=head1 DESCRIPTION

For detailed information see L<Business::OnlineThirdPartyPayment>.

=head1 NOTE

=head1 COMPATIBILITY

This module implements a dummy payment gateway.

=head1 AUTHOR

Jeff Finucane <jeff@cmh.net>

=head1 SEE ALSO

perl(1). L<Business::OnlinePayment>.

=cut

