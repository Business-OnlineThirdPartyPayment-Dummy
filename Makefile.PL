use ExtUtils::MakeMaker;
# See lib/ExtUtils/MakeMaker.pm for details of how to influence
# the contents of the Makefile that is written.
WriteMakefile(
    'NAME'         => 'Business::OnlineThirdPartyPayment::Dummy',
    'VERSION_FROM' => 'Dummy.pm', # finds $VERSION
    'AUTHOR'       => 'Jeff Finucane <jeff@cmh.net>',
    'PREREQ_PM'    => { 
                        'Business::OnlineThirdPartyPayment' => 3,
                        'Business::CreditCard' => 0.27,
                      },
);
