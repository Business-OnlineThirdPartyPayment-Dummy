# Before `make install' is performed this script should be runnable with
# `make test'. After `make install' it should work as `perl eWay.t'

use Test;
BEGIN { plan tests => 6 };
use Business::OnlineThirdPartyPayment::Dummy;

# a test transaction
my ($tx, $txnum, $res);
ok($tx = new Business::OnlineThirdPartyPayment("Dummy"));
ok(
    $tx->content(
        type           => 'CC',
        login          => '87654321',
        action         => 'Authorization Only',
        description    => 'Business::OnlineThirdPartyPayment test',
        amount         => '49.95',
        invoice_number => '100100',
        name           => 'Tofu Beast',
        card_number    => '4646464646464646',
        expiration     => '11/08',
    )
);
ok($tx->test_transaction(1));
ok($tx->submit());
ok($tx->is_success());
ok($tx->authorization(), 'Authorized');

